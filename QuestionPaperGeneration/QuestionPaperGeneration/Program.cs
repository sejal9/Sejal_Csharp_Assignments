﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace QuestionPaperGeneration
{
    class Program
    {
        public static int FindMax(int al, int bl, int cl, int dl)
        {
            int i, max = -1, maxIndex = -1;
            int[] arr = { al, bl, cl, dl };
            for (i = 0; i < 4; i++)
            {
                if (max < arr[i])
                {
                    max = arr[i];
                    maxIndex = i;
                }
            }
            return maxIndex;
        }

        static void Main(string[] args)
        {
            List<string> aset = new List<string> { };
            List<string> bset = new List<string> { };
            List<string> cset = new List<string> { };
            List<string> dset = new List<string> { };

            for (int i = 0; i < 10000; i++)
            {
                aset.Add("A" + i);
            }

            for (int i = 0; i < 10000; i++)
            {
                bset.Add("B" + i);
            }

            for (int i = 0; i < 18000; i++)
            {
                cset.Add("C" + i);
            }

            for (int i = 0; i < 12000; i++)
            {
                dset.Add("D" + i);
            }
            
            List<string> questionSet = new List<string>();
                
            int al = aset.Count;
            int bl = bset.Count;
            int cl = cset.Count;
            int dl = dset.Count;
            int totalLength = al + bl + cl + dl;
            int d = dl - 1;
            int a = al - 1;
            int b = bl - 1;
            int c = cl - 1;

            int position, oldPosition = -1;

            Stopwatch sw = Stopwatch.StartNew();

            position = FindMax(al, bl, cl, dl);

            for (int i = 0; i < totalLength; i++)
            {
                if (position == oldPosition)
                {
                    if (position == 0)
                    {
                        position = FindMax(0, b, c, d);
                    }
                    else if (position == 1)
                    {
                        position = FindMax(a, 0, c, d);
                    }
                    else if (position == 2)
                    {
                        position = FindMax(a, b, 0, d);
                    }
                    else if (position == 3)
                    {
                        position = FindMax(a, b, c, 0);
                    }
                }

                if (position == 0 && a > -1)
                {
                    questionSet.Add(aset[a]);
                    a--;
                }
                else if (position == 1 && b > -1)
                {
                    questionSet.Add(bset[b]);
                    b--;
                }
                else if (position == 2 && c > -1)
                {
                    questionSet.Add(cset[c]);
                    c--;
                }
                else if (position == 3 && d > -1)
                {
                    questionSet.Add(dset[d]);
                    d--;
                }
                oldPosition = position;
                position = FindMax(a, b, c, d);

            }

            sw.Stop();
            Console.WriteLine("Time:" + sw.Elapsed.TotalMilliseconds);

            /*foreach (var item in questionSet)
            {
                Console.WriteLine(item);
            }*/

            Console.ReadKey();
        }
    }
}
